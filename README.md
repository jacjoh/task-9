# task-9

Console application

- Allow a user to insert and retrieve words or numbers from a custom collection called ChaosArray
- When adding and retrieving data it does so from a random index at the array. If you retrieve from an empty index a custom exception is thrown. If you add to a filled index a custom exception is thrown. 
