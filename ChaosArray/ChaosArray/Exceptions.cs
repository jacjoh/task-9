﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    public class AccessingUnavailableSpaceExceptions : Exception
    {
        public AccessingUnavailableSpaceExceptions() { }

        public AccessingUnavailableSpaceExceptions(string message) : base(message) { }

        public AccessingUnavailableSpaceExceptions(string message, Exception inner) : base(message, inner) { }
    }

    public class RetrievingEmptySpaceException : Exception
    {
        public RetrievingEmptySpaceException() { }

        public RetrievingEmptySpaceException(string message) : base(message) { }

        public RetrievingEmptySpaceException(string message, Exception inner) : base(message, inner) { }
    }
}
