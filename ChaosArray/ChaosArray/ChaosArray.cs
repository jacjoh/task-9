﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ChaosArray
{
    public class GenericChaosArray<T>
    {
        private T[] data = new T[10];
        public void AddItem(T t)
        {
            //Items should be added at random position
            Random rnd = new Random();
            int index = rnd.Next(10);

            try
            {
                //If the space is available store the data, else throw exception
                if (data[index] == null)
                {
                    data.SetValue(t, index);
                    Console.WriteLine($"{index} - {t}");
                }
                else
                {
                    throw new AccessingUnavailableSpaceExceptions($"{index} - Could not add " +
                        $"item as the space is already taken at the position");
                }
            } catch(AccessingUnavailableSpaceExceptions ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            
        }

        public void RetrieveItem()
        {
            //Items should be retrieved from a random position
            Random rnd = new Random();
            int index = rnd.Next(10);

            try
            {
                //If it is something stored retrieve it, else throw an exception
                if (data[index] != null)
                {
                    T retrieved = (T)data.GetValue(index);
                    data.SetValue(null, index);
                    Console.WriteLine($"{index} - retrieved {retrieved}");
                }
                else
                {
                    throw new RetrievingEmptySpaceException($"{index} - Could not retrieve as the space is empty");
                }
            }
            catch(RetrievingEmptySpaceException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
