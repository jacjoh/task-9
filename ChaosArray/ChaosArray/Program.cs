﻿using System;
using System.Dynamic;

namespace ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {
            GenericChaosArray<string> chaosArray = new GenericChaosArray<string>();
            Console.WriteLine("To add an item type \"a\" and then what to add");
            Console.WriteLine("To retrieve an item type \"r\"");
            Console.WriteLine("To exit the program type \"e\"");

            string choice = "";
            bool runProgram = true;
            while (runProgram)
            {
                choice = Console.ReadLine();
                switch(choice.Substring(0,1))
                {
                    case "e":
                        runProgram = false;
                        break;
                    case "a":
                        //if it is negative there are no space in the string
                        if(choice.IndexOf(" ") < 0)
                        {
                            Console.WriteLine("Must type \"a\" and the item to add");
                            break;
                        }
                        chaosArray.AddItem(choice.Substring(choice.IndexOf(" ") + 1));
                        break;
                    case "r":
                        chaosArray.RetrieveItem();
                        break;
                    default:
                        break;
                }       
            }
        }
    }
}
